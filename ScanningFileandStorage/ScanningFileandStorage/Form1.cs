﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ScanningFileandStorage
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void getDirectoryButton_Click(object sender, EventArgs e)
        {
            resultsBox.Items.Clear();
            findButton.Enabled = true;

            //getting Directory
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (DialogResult.OK == fbd.ShowDialog())
            {
                pathToSearchCombo.Text = fbd.SelectedPath;
                pathToSearchCombo.Items.Add(fbd.SelectedPath);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            findButton.Enabled = true;
            cancelButton.Enabled = false;
            resultsBox.Items.Clear();
        }

        private void findButton_Click(object sender, EventArgs e)
        {
            cancelButton.Enabled = true;
            String directoryPath = pathToSearchCombo.Text;
            long driveSize = 0;

            try
            {
                DirectoryInfo di = new DirectoryInfo(directoryPath);

                // Getting Files Through Directory
                FileInfo[] fi = di.GetFiles();
                resultsBox.Items.Clear();

                //Title
                resultsBox.Items.Add("Size             Modified  FileName");

                // Getting Drives Size
                DriveInfo[] allDrives = DriveInfo.GetDrives();
                foreach (DriveInfo drive in allDrives)
                {
                    if (drive.IsReady)
                    {
                        driveSize += drive.TotalSize;
                    }
                }

                // Getting each file's data
                foreach (FileInfo f in fi)
                {
                    // Caculating Percentage of File Sizes
                    double percentage = ((double)f.Length / (double)driveSize) * 100;

                    //Show File Datas in List Box
                    resultsBox.Items.Add(string.Format("{0,-14:#,0.00%}{1,12:d} {2}", percentage, f.LastWriteTime, f.Name));
                }
            }

            // Error Message
            catch (Exception ex)
            {
                MessageBox.Show("Please browse file directory");
            }
        }
    }
}
