﻿namespace ScanningFileandStorage
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pathToSearchCombo = new System.Windows.Forms.ComboBox();
            this.getDirectoryButton = new System.Windows.Forms.Button();
            this.findButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.resultsBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // pathToSearchCombo
            // 
            this.pathToSearchCombo.FormattingEnabled = true;
            this.pathToSearchCombo.Location = new System.Drawing.Point(42, 26);
            this.pathToSearchCombo.Name = "pathToSearchCombo";
            this.pathToSearchCombo.Size = new System.Drawing.Size(271, 21);
            this.pathToSearchCombo.TabIndex = 4;
            // 
            // getDirectoryButton
            // 
            this.getDirectoryButton.Location = new System.Drawing.Point(344, 26);
            this.getDirectoryButton.Name = "getDirectoryButton";
            this.getDirectoryButton.Size = new System.Drawing.Size(32, 23);
            this.getDirectoryButton.TabIndex = 5;
            this.getDirectoryButton.Text = "...";
            this.getDirectoryButton.UseVisualStyleBackColor = true;
            this.getDirectoryButton.Click += new System.EventHandler(this.getDirectoryButton_Click);
            // 
            // findButton
            // 
            this.findButton.Location = new System.Drawing.Point(42, 68);
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size(101, 23);
            this.findButton.TabIndex = 7;
            this.findButton.Text = "Find!";
            this.findButton.UseVisualStyleBackColor = true;
            this.findButton.Click += new System.EventHandler(this.findButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Enabled = false;
            this.cancelButton.Location = new System.Drawing.Point(178, 68);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(101, 23);
            this.cancelButton.TabIndex = 8;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // resultsBox
            // 
            this.resultsBox.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultsBox.FormattingEnabled = true;
            this.resultsBox.ItemHeight = 11;
            this.resultsBox.Location = new System.Drawing.Point(42, 109);
            this.resultsBox.Name = "resultsBox";
            this.resultsBox.Size = new System.Drawing.Size(612, 213);
            this.resultsBox.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 346);
            this.Controls.Add(this.resultsBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.findButton);
            this.Controls.Add(this.getDirectoryButton);
            this.Controls.Add(this.pathToSearchCombo);
            this.Name = "Form1";
            this.Text = "FileEnumeration";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox pathToSearchCombo;
        private System.Windows.Forms.Button getDirectoryButton;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ListBox resultsBox;
    }
}

