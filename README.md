# README #

ScanningFiles Program(R) Version 1.0 21/09/2014

### What is this repository for? ###

* An App which can scan files data (such as names, storage occupancy and modified dates) in a specific directory.
* Version 1.0

### How do I get set up? ###

* Support Microsoft Window XP/7/8
* Open Question 3 Folder. Run ScanningFileandStorage.exe.

### Contribution guidelines ###

* Choose Directory and Get File Names, Storage and Modified Date. File Storage will show in Pecentage.

### Who do I talk to? ###

* Author : Phyo Thant Ko
* Email : phyothantko.tp@gmail.com